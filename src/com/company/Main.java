package com.company;

public class Main {

    public static void main(String[] args) {
	    Stack stack = new Stack();
	    stack.push("1");
	    stack.push("2");
	    stack.push("3");
	    stack.push("item");

        System.out.println(stack.pop());
        System.out.println(stack.get());

		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
    }
}
