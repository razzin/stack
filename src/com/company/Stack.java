package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Stack implements  StackOperations{

    private List<String> items = new ArrayList<>();

    // pobiera wszystkie elementy stosu - 1 el listy to szczyt stosu
    @Override
    public List<String> get() {
        List<String> returns = new ArrayList<>();
        for(int i=1;i<=items.size();i++){
            returns.add(items.get(items.size()-i));
        }
        return returns;
    }
    // pobiera element ze stosu
    @Override
    public Optional<String> pop() {
        if (items.isEmpty()) {
            return Optional.empty();
        }
        String top = items.get(items.size() - 1);
        items.remove(items.size() - 1);
        return Optional.ofNullable(top);
    }

    // dodaje element na stos
    @Override
    public void push(String item) {
        items.add(item);
    }
}
